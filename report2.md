# 实验二：函数的应用

**1. 实验目的与要求**

（1）理解模块化程序设计思想；

（2）学会函数的定义与调用方法，理解函数的调用过程及参数传递；

（3）在程序设计中能灵活运用函数解决实际问题。

**2. 实验内容**

（1）根据实际问题，模块划分练习；

（2）函数的定义、调用与函数的声明 

（3）函数的嵌套调用；

（4）递归函数设计。


## 问题1：字符图案打印机

### 1. 问题陈述

编写程序，通过函数方便地打印各种字符组成的图案，如：矩形、平行四边形、直角三角形、金
字塔（等腰三角）形等图案。要求图案的大小、组成图案的字符都可以灵活改变。
```
***** ***** * *
***** ***** ** ***
***** ***** *** *****
***** ***** **** *******
***** ***** ***** *********
```

### 2. 输入输出描述

- 输入：图案形状，大小和组成字符
- 输出：字符组成的图案


### 3. 手动演算示例

示例 1：打印矩形。调用函数 `print_rectangle(5, 10, '*')` ，打印出一个 5 行 10 列，由字符 `*` 组
成的矩形图案：
```
**********
**********
**********
**********
**********
```

### 4. 算法设计

#### （1）打印换行符

函数 `println()` 打印一个换行符。
```c
void println(void)
{
    putchar('\n');
}
```

#### （2）重复打印字符

函数 `print_n_chars(n, c)` 重复打印 `n` 个字符 `c`。
```c
void print_n_chars(int n, char c)
{
    for (int i = 0; i < n; i++) {
        putchar(c);
    }
}
```


### 5. 测试

### 总结


## 问题1：整数的特殊性质

### 1. 问题陈述

给定一个正整数 $n$，判断这个数是否具一系列特殊性质，如：奇数（odd number）、偶数（even number）、素数（prime number）、完数（perfect number）、回文数（palindrome number）和水仙花数（narcissistic number）。要求使用函数判断数的性质。

### 2. 输入和输出

- 输入：正整数 $n$
- 输出：正整数 $n$ 具有的性质

### 3. 演算示例

示例1：$n=1$

输出结果
```
odd = YES
even = NO
prime = NO
perfect = NO
palindrome = YES
narcissistic = NO 
```

示例2：$n=2$

输出结果
```
odd = YES
even = NO
prime = NO
perfect = NO
palindrome = YES
narcissistic = NO 
```

### 4. 算法设计

程序的总体思路是：输入一个正整数 n，判断其特殊性质并输出结果。
```
输入正整数 n
判断 n 是否是奇数
判断 n 是否是偶数
判断 n 是否是素数
判断 n 是否是完数
判断 n 是否是回文数
判断 n 是否是水仙花数
```

对以上总体思路进行细化：
```
输入正整数 n

// 判断 n 是否是奇数
IF odd(n) THEN // n 为奇数
    Print "odd = YES"
ELSE
    Print "odd = NO"
END IF

// 判断 n 是否是偶数
IF even(n) THEN
    Print "even = YES"
ELSE
    Print "even = NO"
END IF

...

```

下面给出每个函数的伪代码及其实现：
```
// 判断 n 是否是奇数
FUNCTION odd(n)
    INPUT: 正整数 n
    OUTPUT: true，若 n 是奇数；否则 false

    IF n % 2 == 1 THEN
        RETURN true
    ELSE
        RETURN false
    END IF
END FUNCTION
```
对应的 C 语言代码：
```c
// 判断 n 是否是奇数
bool odd(int n)
{
    if (n % 2 == 1)
        return true;
    else
        return false;
}
```


```
// 判断 n 是否是偶数
FUNCTION even(n)
    INPUT: 正整数 n
    OUTPUT: true，若 n 是偶数；否则 false

....

END FUNCTION
```

### 5. 测试

### 总结



